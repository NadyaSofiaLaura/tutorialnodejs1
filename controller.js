'use strict';

var response = require('./res');
var connection = require('./conn');

exports.users = function(req, res) {
    connection.query('SELECT * FROM person', function (error, rows, fields){
        if(error){
            console.log(error)
        } else{
            response.ok(rows, res)
        }
    });
};

exports.index = function(req, res) {
    response.ok("Hello from Laura!", res)
};

exports.createUsers = function(req, res) {
    
    var first_name = req.body.first_name;
    var last_name = req.body.last_name;

    connection.query('INSERT INTO person (first_name, last_name) values (?,?)',
    [ first_name, last_name ], 
    function (error, rows, fields){
        if(error){
            console.log(error)
        } else{
            response.ok("User successfully created!", res)
        }
    });
};

//  exports.findUsers = function(req, res) {
    
//      var id = req.params.id;

//      connection.query('SELECT * FROM person where id = ?',
//      [ id ], 
//      function (error, rows, fields){
//          if(error){
//              console.log(error)
//          } else{
//              response.ok(rows, res)
//          }
//      });
//  };


exports.updateUsers = function(req, res) {
    
    var id = req.body.id;
    var first_name = req.body.first_name;
    var last_name = req.body.last_name;

    connection.query('UPDATE person SET first_name = ?, last_name = ? WHERE id = ?',
    [ first_name, last_name, id ], 
    function (error, rows, fields){
        if(error){
            console.log(error)
        } else{
            response.ok("User Updated!", res)
        }
    });
};


exports.deleteUsers = function(req, res) {
    
    var id = req.body.id;

    connection.query('DELETE FROM person WHERE id = ?',
    [ id ], 
    function (error, rows, fields){
        if(error){
            console.log(error)
        } else{
            response.ok("User Deleted!", res)
        }
    });
};


exports.createContact = function(req, res) {
    
    var id = req.body.id;
    var phone_number = req.body.phone_number;

    connection.query('INSERT INTO contact (id, phone_number) values (?,?)',
    [ id, phone_number ], 
    function (error, rows, fields){
        if(error){
            console.log(error)
        } else{
            response.ok("Phone Number created!", res)
        }
    });
};

//   exports.findContact = function(req, res) {
    
//       var id = req.params.id;

//       connection.query('SELECT * FROM contact where id = ?',
//       [ id ], 
//       function (error, rows, fields){
//           if(error){
//               console.log(error)
//           } else{
//               response.ok(rows, res)
//           }
//       });
//   };

function findContact(id){
    return new Promise(resolve => {
        var query = 'SELECT * FROM contact where id = ?';
        connection.query(query,
        [ id ], 
        function (error, rows, fields){
            if(error){
                console.log(error)
            } else{
                resolve(rows); //Kembalian berupa kontak data
            }
        });
    });
}

 exports.findUsers = async function(req, res) {

     var id = req.params.id;

     let log = await findContact(id);
     console.log(log);

     var query = 'SELECT * FROM person where id = ?';
     connection.query(query, [ id ],
     function (error, rows, fields){
         if(error){
             console.log(error)
         } else{
             return response.ok(rows, res)
         }
     });
 };


//  'use strict';

// var response = require('./res');
// var connection = require('./conn');

exports.createProduct = function(req, res) {

    var product_name = req.body.product_name;
    var stock = req.body.stock;
    var id = req.body.id;

    var promise1 = new Promise(function(resolve, reject) {
        connection.query('SELECT * FROM users where id = ?',
            [ id ], function (error, rows, fields){
            if(error){
                console.log(error)
            }

            resolve(rows);
        });
    });

    promise1.then(function (resolve, reject) {

        if(resolve[0].is_admin !== "1"){
            response.ok("Prohibited!",res);
            return false;
        }

        var promise2 = new Promise(function(resolve, reject) {
            connection.query('INSERT INTO barang (name, stock) values (?,?)',
                [ product_name, stock ], function (error, rows, fields){
                    if(error){
                        console.log(error)
                    }

                    resolve(rows.affectedRows > 0);
                });
        });

        return promise2;

    }).then(function (result) {
        console.log(result);
        (result) ? response.ok("Success",res) : response.ok("failed",res);

        return true;
    });
};
